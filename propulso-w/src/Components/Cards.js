import React from 'react'
import s from '../styles/Cards.module.css'
import Producto from '../images/producto.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'


const Cards = () => {
    return (
        <div className={s.container}>
            <div className={s.containerImage}>
                <img src={Producto} alt="producto" />
                <span className={s.productoNew}>new</span>
                <span className={s.productoDescuento}>20%</span>
            </div>
            <div className={s.CardContainer}>
                <p className={s.NameProduct}>Nombre de producto ejemplo largo</p>
                <p className={s.ProductPrice}>
                    <span>$19.990 </span>
                    <span>$9.990</span>
                </p>
                <div className={s.BuyButton} href="#"><FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon>Comprar</div>
            </div>
        </div>
    )
}

export default Cards