import React from 'react'
import Especialistas from '../Sections/Especialistas'
import Subscribe from '../Sections/Subscribe'
import Header from '../Sections/Header'
import Products from '../Sections/Products'

const Home = () => {
  return (
      <>
    <Header></Header>
    <Products></Products>
    <Especialistas></Especialistas>
    <Subscribe></Subscribe>
      </>
  )
}

export default Home