import React from 'react'
import Logo from '../images/logo.png'
import Slider from '../images/slider.png'
import s from '../styles/Header.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faBagShopping, faMagnifyingGlass} from '@fortawesome/free-solid-svg-icons'


const Header = () => {
  return (
      <>
    <div className={s.TopBar}>Mensaje Informativo - Comercial</div>
        <div className={s.container}>
            <div className={s.navBar}>
                <img width="150" src={Logo} alt="Logo" />
                <ul className={s.navBarList}>
                    <li className={s.navBarItem}><a href="#">Nosotros</a></li>
                    <li className={s.navBarItem}><a href="#">Galeria</a></li>
                    <li className={s.navBarItem}><a href="#">Servicios</a></li>
                    <li className={s.navBarItem}><a href="#">Productos</a></li>
                    <li className={s.navBarItem}><a href="#">Contacto</a></li>
                    <li className={s.navBarItem}><a href="#"><FontAwesomeIcon icon={faMagnifyingGlass}></FontAwesomeIcon></a></li>
                    <li className={s.navBarItem}><a href="#"><FontAwesomeIcon icon={faBagShopping}></FontAwesomeIcon></a></li>
                </ul>
            </div>

            <div className={s.Slider}>
                <a href="#">
                    <img width="100%" src={Slider} alt="slider" />
                </a>
            </div>
        </div>
      </>
  )
}

export default Header