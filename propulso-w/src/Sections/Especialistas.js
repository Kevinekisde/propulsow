import React from 'react'
import Especialistas1 from '../images/especialistas-1.png'
import Especialistas2 from '../images/especialistas-2.png'
import Especialistas3 from '../images/especialistas-3.png'
import Especialistas4 from '../images/especialistas-4.png'
import s from '../styles/Especialistas.module.css'

const Especialistas = () => {
  return (
    <div className={s.container}>
        <h2>Profesionales Especialistas</h2>
        <div className={s.containerImages}>
            <img className={s.Top} src={Especialistas4} alt=""/>
            <img classNames={s.grande} src={Especialistas2} alt=""/>
            <img className={s.chico} src={Especialistas3} alt=""/>
            <img className={s.chico2} src={Especialistas1} alt=""/>
        </div>
    </div>
  )
}

export default Especialistas