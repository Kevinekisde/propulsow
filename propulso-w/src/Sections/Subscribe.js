import React from 'react'
import s from '../styles/Subscribe.module.css'

const Subscribe = () => {
  return (
    <div className={s.Container}>
            <div className={s.InfoContainer}>
                <div className={s.textContainer}>
                    <h3>Subscribe to Newsletters</h3>
                    <p>Be aware of upcoming sales and events. Receive gifts and special offers!</p>
                </div>

                <div className={s.inputContainer}>
                    <input className={s.input} type="text" placeholder="Ingresa tu Correo"/>
                    <button className={s.SubscribeButton} type="submit">Subscribe</button>
                </div>
            </div>
        </div>
  )
}

export default Subscribe